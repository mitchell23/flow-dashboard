function heartbeat() {
  clearTimeout(this.pingTimeout);
  // Use `WebSocket#terminate()`, which immediately destroys the connection,
  // instead of `WebSocket#close()`, which waits for the close timer.
  // Delay should be equal to the interval at which your server
  // sends out pings plus a conservative assumption of the latency.
  this.pingTimeout = setTimeout(() => {
    this.terminate();
  }, 30000 + 1000);
}

  function connectToWebSocket(url, ticker){
    console.log(url);
    if(!client.current){
      client.current = new W3CWebSocket(url);
      client.current.onopen = () => {
        client.current.send(ticker);
      };
      client.current.onmessage = (message) => {
        let data = JSON.parse(message.data);
        updateRawData(data);
        return client.close;
      }
    }else{
      console.log('trying to reopen exisitng socket');
      }
};

function convertUnixTimestamp(timestamp){
  var date = new Date(timestamp);
  // Hours part from the timestamp
  var hours = date.getHours();
  // Minutes part from the timestamp
  var minutes = "0" + date.getMinutes();
  // Seconds part from the timestamp
  var seconds = "0" + date.getSeconds();
  // Will display time in 10:30:23 format
  var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
  return formattedTime;
}

function formatData(data, tickerData, tickerQuoteData, updateTickerQuoteData, updateMarketSentiment, updateTickerData, marketSentimentHistory, updateMarketSentimentHistory){
  let currentTimestamp = data.t;
  let tradeSentiment;
  let mark = (tickerQuoteData[0] + tickerQuoteData[1]) / 2;
  //if incoming data is a quote
  if(data.ev === 'Q'){
    updateTickerQuoteData([data.ap, data.bp, data.t]);
    let currentMark = tickerQuoteData;
  }else if(data.ev === "T"){
    // console.log('New Trade In');
    let currentMark = tickerQuoteData;
    if(data.p === currentMark[0]){
      tradeSentiment = 'atAsk: ' + tickerQuoteData[0] ;
      updateMarketSentiment(marketSentiment => marketSentiment + (1 * data.s))
    }else if(data.p > currentMark[0]){
      tradeSentiment = 'aboveAsk: ' + tickerQuoteData[0];
      updateMarketSentiment(marketSentiment => marketSentiment + (2 * data.s))
    }else if(data.p === currentMark[1]){
      tradeSentiment = 'atBid: ' + tickerQuoteData[1];
      updateMarketSentiment(marketSentiment => marketSentiment - (1 * data.s))
    }else if(data.p < currentMark[1]){
      tradeSentiment = 'underBid: ' + tickerQuoteData[1];
      updateMarketSentiment(marketSentiment => marketSentiment - (2 * data.s))
    }else if(data.p < mark){
      tradeSentiment = 'underMark: ' + mark;
      updateMarketSentiment(marketSentiment => marketSentiment - (0.5 * data.s))
    }else if(data.p > mark){
      tradeSentiment = 'aboveMark: ' + mark;
      updateMarketSentiment(marketSentiment => marketSentiment + (0.5 * data.s))
    }else{
      tradeSentiment = 'mark: ' + mark;
    }
    let currentSaleData = [data.sym, data.p, data.s, tradeSentiment, convertUnixTimestamp(data.t), data.i];
    updateTickerData(tickerData => [currentSaleData, ...tickerData.slice(0, 50)]);
    // console.log('New Trade In');
  }else if(data.ev === "AM"){
    //do something heree to trigger an action to store previous candle open - close - change and sentiment
    updateMarketSentimentHistory(marketSentimentHistory => [[data.o, data.c, (data.c - data.o), data.v, marketSentiment], ...marketSentimentHistory])
    console.log(data);
    updateMarketSentiment(0);
  }
}
