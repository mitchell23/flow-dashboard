import React, { useState, useRef, useCallback } from 'react';
import Sidebar from './components/Sidebar/Sidebar';
import SelectedTickerInfo from './components/SelectedTickerInfo/SelectedTickerInfo';
import SelectedTickerFlow from './components/SelectedTickerFlow/SelectedTickerFlow';
import OptionsFlow from './components/OptionsFlow/OptionsFlow';
import LoginButton from './components/LoginButton/LoginButton';
import LogoutButton from './components/LogoutButton/LogoutButton';
import Profile from './components/Profile/Profile';

import classes from "./App.module.css";


export default function App(){
  const [selectedTicker, updateSelectedTicker] = useState('');
  const [tickerList, updateTickerList] = useState(['SPY', 'SPX', 'TSLA', 'AMZN']);
  const [tickerData, updateTickerData] = useState([]);
  const [optionsData, updateOptionsData] = useState([]);
  const [tickerQuoteData, updateTickerQuoteData] = useState([0,0,0]);
  const [marketSentiment, updateMarketSentiment] = useState(0);
  const [marketSentimentHistory, updateMarketSentimentHistory] = useState([]);
  const client = useRef();

  const getUniqueID = () => {
    const s4 = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    return s4() + s4() + '-' + s4();
  };

  const addTickerData = useCallback((newTickerData) => {
    updateTickerData(tickerData => [newTickerData, ...tickerData]);
  }, [updateTickerData]);

  const addTickerQuoteData = useCallback((newTickerQuoteData) => {
    updateTickerQuoteData(newTickerQuoteData);
  }, [updateTickerQuoteData]);




    function connectToWebSocket(ticker){
      var userID = getUniqueID();
      console.log(userID);
      // var url = `ws://polygon-data-webhook-portal-kxqub.ondigitalocean.app/${userID}/${ticker}`
      var url = `ws://polygon-data-webhook-portal-kxqub.ondigitalocean.app`
      console.log(url);


      if(!client.current){
        client.current.onopen = () => {
          client.current.send(JSON.stringify({ticker: ticker, userID: userID, status: 'open'}));
        };

        client.current.onmessage = (message) => {
          let data = JSON.parse(message.data);
          // console.log(data)
          if(data.dataType){
            console.log('options contract')
            updateOptionsData(optionsData => [data, ...optionsData]);
          }else{
            // formatData(data, tickerQuoteData, updateTickerQuoteData, marketSentiment, updateMarketSentiment);
          }
        };

        client.current.onclose = () => {
          client.current.send(JSON.stringify({ticker: ticker, userID: userID, status: 'close'}));
          client.current.close();
        };

        return () => {
          client.current.close();
        };

      }else{
        console.log('trying to reopen exisitng socket');
        }
    };

  var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  });



    return (
      <div className={classes.App} style={{width: '100%', display: 'flex', alignItems: 'flex-start', justifyContent: 'center',flexDirection: 'row', background: '#2b2b2b'}}>
        <LoginButton />
        <LogoutButton />
        <Profile />
        <Sidebar updateSelectedTicker={updateSelectedTicker} tickerList={tickerList} updateTickerList={updateTickerList} />

        <div style={{width: '90%', display: 'flex', alignItems: 'center', justifyContent: 'center',flexDirection: 'column', background: '#2b2b2b', boxSizing: 'border-box'}}>
            {
              //<SelectedTickerInfo ticker={selectedTicker} tickerQuoteData={tickerQuoteData} marketSentiment={marketSentiment} marketSentimentHistory={marketSentimentHistory} />
            }
          <div style={{width: '100%', display: 'flex', flexDirection: 'row'}}>
            <SelectedTickerFlow selectedTicker={selectedTicker} />
            <OptionsFlow selectedTicker={selectedTicker} />
          </div>
        </div>
      </div>
    );
}
