import React, {useState, useEffect, useMemo} from 'react';
import axios from 'axios';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { DataGridPro, GridColDef, LicenseInfo } from '@mui/x-data-grid-pro';

LicenseInfo.setLicenseKey(
  '8dc57413e860b14049973be701176cc5T1JERVI6MzQ4NzksRVhQSVJZPTE2NzIwMDQzMDQwMDAsS0VZVkVSU0lPTj0x',
);

const optionsColumns: GridColDef[] = [
  { field: 'created_at', headerName: 'Time', width: 200 },
  { field: 'contractTicker', headerName: 'Ticker', width: 100 },
  { field: 'contractStrike', headerName: 'Strike', width: 100 },
  { field: 'contractType', headerName: 'C/P', width: 100 },
  { field: 'contractExipration', headerName: 'Expiration', width: 150 },
  { field: 'contractPrice', headerName: 'Price', width: 150 },
  { field: 'tradeSize', headerName: 'Size', width: 150 },
  { field: 'totalTradeValue', headerName: 'Value', width: 150 },
  { field: 'tradeComment', headerName: 'Comment', width: 250 },
];


export default function OptionsFlow(props){
  const [optionsFlow, updateOptionsFlow] = useState([])
  const [lotFilter, updateLotFilter] = useState(0);
  const [optionsSentiment, updateOptionsSentiment] = useState({calls: 0, puts: 0});

  useEffect(() => {
    let optionsURL = `https://polygon-api-6i93m.ondigitalocean.app/api/v1/optionsFlow/${props.selectedTicker}`;
    if(props.selectedTicker !== ''){
      optionsURL = `https://polygon-api-6i93m.ondigitalocean.app/api/v1/optionsFlow/${props.selectedTicker}/100`
    }
     axios.get(optionsURL)
       .then(function (response) {
         let callSentiment = 0;
         let putSentiment = 0;
         let totalValue = 0;
         updateOptionsFlow(response.data)
         for(const flow of response.data) {
           if(flow.contractType === 'CALL'){
             callSentiment += Number(flow.contractPrice.substring(1)) * Number(flow.tradeSize);
           }else{
             putSentiment += Number(flow.contractPrice.substring(1)) * Number(flow.tradeSize);
           }
         }

         updateOptionsSentiment({calls: (callSentiment / (callSentiment + putSentiment)).toFixed(2) * 100 + '%' , puts: (putSentiment / (callSentiment + putSentiment)).toFixed(2) * 100 + '%' })

       })
       .catch(function (error) {
         console.log(error);
       })
     let refreshInterval = setInterval(() => {
       axios.get(optionsURL)
         .then(function (response) {
           let callSentiment = 0;
           let putSentiment = 0;
           let totalValue = 0;
           updateOptionsFlow(response.data)
           for(const flow of response.data) {
             if(flow.contractType === 'CALL' && flow.contractPrice){
               callSentiment += Number(flow.contractPrice.substring(1)) * Number(flow.tradeSize);
             }else if (flow.contractType === 'PUT' && flow.contractPrice){
               putSentiment += Number(flow.contractPrice.substring(1)) * Number(flow.tradeSize);
             }
           }

           updateOptionsSentiment({calls: (callSentiment / (callSentiment + putSentiment)).toFixed(2) * 100 + '%' , puts: (putSentiment / (callSentiment + putSentiment)).toFixed(2) * 100 + '%' })

         })
         .catch(function (error) {
           console.log(error);
         })
     }, 10000)

     return(() => {
       console.log('interval cleared');
       clearInterval(refreshInterval);
     })

   },[props.selectedTicker])



  return useMemo(
    () => (
    <div style={{display: 'flex', alignItems: 'flex-start', justifyContent: 'flex-start',flexDirection: 'row', height: '600px', width: '100%', boxSizing: 'border-box', padding: '20px 10px', color: '#fff '}}>
      <div style={{display: 'flex', flexDirection:'column', height: '100%', width: '100%', marginTop: '10px' }}>
        <div style={{display: 'flex', flexDirection:'row', width: '100%', marginTop: '10px' }}>
          <p>Calls: {optionsSentiment.calls} </p>
          <br />
          <p>Puts: {optionsSentiment.puts}</p>
        </div>
        <div style={{flexGrow: 1}}>
          <DataGridPro
            getRowId={(r) => r._id}
            style={{color: '#fff'}}
            rows={optionsFlow}
            columns={optionsColumns}
            throttleRowsMs={0}
            rowHeight={38}
            disableSelectionOnClick
             />
        </div>
      </div>
    </div>
  ), [optionsFlow, lotFilter]
  );
}
