import React, { useState, useEffect } from 'react';
import classes from './Sidebar.module.css';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';


import SidebarTickerButton from './SidebarTickerButton/SidebarTickerButton';

export default function Sidebar(props){
  const [tickerInput, updateTickerInput] = useState('')

  useEffect(()=>{
    updateTickerInput('');
  },[props.tickerList])

  return(
    <div className={classes.Sidebar}>
      <div>
        <span style={{color: '#F3CB56'}}>LYNX</span>
      </div>
      <div>
        <h3 style={{color: '#fff'}}>Ticker List</h3>
      </div>
      <TextField required value={tickerInput} onChange={(e) => updateTickerInput(e.target.value)} id="outlined-basic" label="Ticker" color="success" variant="outlined" focused style={{marginBottom: '5px'}} />
      <Button variant="contained" onClick={() => props.updateTickerList([...props.tickerList, tickerInput])}>Add Ticker</Button>
      <Divider />
      <List>
      <SidebarTickerButton key={' '}  connectToWebSocket={props.connectToWebSocket} updateSelectedTicker={props.updateSelectedTicker} />
        {props.tickerList.map((tickerData) =>   <SidebarTickerButton key={tickerData} ticker={tickerData} connectToWebSocket={props.connectToWebSocket} updateSelectedTicker={props.updateSelectedTicker} />)}
      </List>

    </div>
  )
}
