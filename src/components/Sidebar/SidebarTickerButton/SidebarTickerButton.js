import React from 'react';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';

export default function SidebarTickerButton(props){

  function updateSelectedTicker(props){
    props.updateSelectedTicker(props.ticker || '');
  }


  return(
    <ListItem disablePadding>
      <ListItemButton onClick={() => updateSelectedTicker(props)}>
        <ListItemText primary={props.ticker || 'View All'} />
      </ListItemButton>
    </ListItem>
  )
}
