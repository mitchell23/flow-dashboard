import React from 'react';
import classes from './Trade.module.css';

export default function Trade(props){
let tradeSentiment;

if(props.whale){
  tradeSentiment = classes.whale;
}else if(props.data[3].includes('aboveAsk')){
  tradeSentiment = classes.overAsk;
}else if(props.data[3].includes('atAsk')){
  tradeSentiment = classes.atAsk;
}else if(props.data[3].includes('atBid')){
  tradeSentiment = classes.atBid;
}else if(props.data[3].includes('underBid')){
  tradeSentiment = classes.underBid;
}else if(props.data[3].includes('aboveMark')){
  tradeSentiment = classes.atAsk;
}else if(props.data[3].includes('underMark')){
  tradeSentiment = classes.atBid;
}else{
  tradeSentiment = classes.mark;
}

  return(
    <tr className={tradeSentiment} key={props.index}>
      <td>{props.data[1]}</td>
      <td>{props.data[2]}</td>
      <td>{props.data[3]}</td>
      <td>{props.data[4]}</td>
      <td>{props.data[5]}</td>
    </tr>
  )

}
