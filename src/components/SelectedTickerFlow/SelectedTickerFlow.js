import React, {useState, useEffect, useMemo, useCallback} from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { DataGridPro, GridColDef, LicenseInfo } from '@mui/x-data-grid-pro';
import socketIOClient from "socket.io-client";

LicenseInfo.setLicenseKey(
  '8dc57413e860b14049973be701176cc5T1JERVI6MzQ4NzksRVhQSVJZPTE2NzIwMDQzMDQwMDAsS0VZVkVSU0lPTj0x',
);

const tradeColumns: GridColDef[] = [
  { field: 'price', headerName: 'Price', width: 200, sortable: false, filterable: false },
  { field: 'size', headerName: 'Size', width: 100, sortable: false, filterable: false  },
  { field: 'sentiment', headerName: 'Sentiment', width: 200, sortable: false, filterable: false  },
  { field: 'value', headerName: 'Value', width: 150, sortable: false, filterable: false  },
  { field: 'timestamp', headerName: 'Timestamp', width: 100, sortable: false, filterable: false  },
];

// function formatData(data, tickerQuoteData, updateTickerQuoteData, updateMarketSentiment){
//   //if incoming data is a quote
//   if(data.ev === 'Q'){
//     addTickerQuoteData([data.ap, data.bp, data.t]);
//   }else if(data.ev === "T"){
//     let currentSaleData = { id: data.i, ticker: data.sym , price: data.p, size: data.s, sentiment: 'not currently working', sentimentStyle: 'whale', value: formatter.format(data.p * data.s), timestamp: data.t };
//     addTickerData(currentSaleData);
//   }else if(data.ev === "AM"){
//     //do something heree to trigger an action to store previous candle open - close - change and sentiment
//     // updateMarketSentimentHistory(marketSentimentHistory => [[data.o, data.c, (data.c - data.o), data.v, marketSentiment], ...marketSentimentHistory])
//     // updateMarketSentiment(0);
//   }
// }

var formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
});

export default function SelectedTickerFlow(props){
  const [tapeFlow, updateTapeFlow] = useState([])
  const [lotFilter, updateLotFilter] = useState(0);

  const addTickerData = useCallback((newTickerData) => {
    let data = newTickerData;
    if(data.ev === 'T'){
      let currentSaleData = { id: data.i, ticker: data.sym , price: data.p, size: data.s, sentiment: 'not currently working', sentimentStyle: 'whale', value: formatter.format(data.p * data.s), timestamp: data.t };
      // console.log(tapeFlow.length);
      updateTapeFlow(tapeFlow => [currentSaleData, ...tapeFlow]);
    }else{
      // console.log(data);
    }
  }, [updateTapeFlow]);
  useEffect(() =>{
    if(tapeFlow.length >= 200){
      let sliceTape = tapeFlow.slice(0, 100);
      updateTapeFlow(sliceTape);
    }
  },[tapeFlow])

  useEffect(() => {
    updateTapeFlow([]);
    console.log(props.selectedTicker);
    if(props.selectedTicker !== ''){
      const socket = socketIOClient('https://polygon-data-webhook-portal-kxqub.ondigitalocean.app:8010', {
        reconnection: false,
        query: {"ticker": props.selectedTicker}
      });

      console.log(socket);
      console.log('connect to socket');
      socket.on('connectToWebSocket', data => {
        // console.log(data);
        addTickerData(data);
      });


    // CLEAN UP THE EFFECT
    return () => {
      console.log('disconnect');
      socket.disconnect()
    };
    }
}, [props.selectedTicker, addTickerData]);


  return useMemo(
    () => (
    <div style={{display: 'flex', alignItems: 'flex-start', justifyContent: 'flex-start',flexDirection: 'row', height: '600px', width: '100%', boxSizing: 'border-box', padding: '20px 10px', color: '#fff '}}>
      <div style={{display: 'flex', flexDirection:'column', height: '100%', width: '100%', marginTop: '10px' }}>
      <FormControl style={{width: 300}}>
          <InputLabel style={{color: '#fff'}} id="demo-simple-select-label">Lot Size Filter</InputLabel>
          <Select
          style={{color: '#fff'}}
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={lotFilter}
            label="Lot Size Filter"
            onChange={(e) => updateLotFilter(e.target.value)}
          >
            <MenuItem value={0}>0+</MenuItem>
            <MenuItem value={100}>100+</MenuItem>
            <MenuItem value={500}>500+</MenuItem>
            <MenuItem value={1000}>1000+</MenuItem>
            <MenuItem value={2000}>2000+</MenuItem>
          </Select>
        </FormControl>
        <div style={{flexGrow: 1}}>
          <DataGridPro
          getRowId={(r) => r.id}
            style={{color: '#fff'}}
            rows={tapeFlow}
            columns={tradeColumns}
            throttleRowsMs={1000}
            rowHeight={38}
            disableSelectionOnClick
             />
        </div>
      </div>
    </div>
  ), [tapeFlow, lotFilter]
  );
}
