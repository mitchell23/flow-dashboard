import React, {useState, useEffect, useMemo} from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { DataGridPro, GridColDef, LicenseInfo } from '@mui/x-data-grid-pro';
import socketIOClient from "socket.io-client";

LicenseInfo.setLicenseKey(
  '8dc57413e860b14049973be701176cc5T1JERVI6MzQ4NzksRVhQSVJZPTE2NzIwMDQzMDQwMDAsS0VZVkVSU0lPTj0x',
);

const tradeColumns: GridColDef[] = [
  { field: 'price', headerName: 'Price', width: 200, sortable: false, filterable: false },
  { field: 'size', headerName: 'Size', width: 100, sortable: false, filterable: false  },
  { field: 'sentiment', headerName: 'Sentiment', width: 200, sortable: false, filterable: false  },
  { field: 'value', headerName: 'Value', width: 150, sortable: false, filterable: false  },
  { field: 'timestamp', headerName: 'Timestamp', width: 100, sortable: false, filterable: false  },
];


export default function SelectedTickerFlow(props){
  const [tapeFlow, updateTapeFlow] = useState([])
  const [lotFilter, updateLotFilter] = useState(0);

  useEffect(() => {
    console.log(props.selectedTicker);
    if(props.selectedTicker !== ''){
      var socket = socketIOClient(`https://polygon-data-webhook-portal-kxqub.ondigitalocean.app/`);
      console.log('connect to socket');
      socket.on("FromAPI", data => {
        updateTapeFlow(data);
      });
    }


    // CLEAN UP THE EFFECT
    return () => {
      console.log('disconnect');
      socket.disconnect()
    };
  }, [props.selectedTicker]);

  useEffect(() => {
    console.log(tapeFlow)
  }, [tapeFlow])

  return useMemo(
    () => (

    <div style={{display: 'flex', alignItems: 'flex-start', justifyContent: 'flex-start',flexDirection: 'row', height: '600px', width: '100%', boxSizing: 'border-box', padding: '20px 10px', color: '#fff '}}>


      <div style={{display: 'flex', flexDirection:'column', height: '100%', width: '100%', marginTop: '10px' }}>
      <FormControl style={{width: 300}}>
          <InputLabel style={{color: '#fff'}} id="demo-simple-select-label">Lot Size Filter</InputLabel>
          <Select
          style={{color: '#fff'}}
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={lotFilter}
            label="Lot Size Filter"
            onChange={(e) => updateLotFilter(e.target.value)}
          >
            <MenuItem value={0}>0+</MenuItem>
            <MenuItem value={100}>100+</MenuItem>
            <MenuItem value={500}>500+</MenuItem>
            <MenuItem value={1000}>1000+</MenuItem>
            <MenuItem value={2000}>2000+</MenuItem>
          </Select>
        </FormControl>
        <div style={{flexGrow: 1}}>
          <DataGridPro
            style={{color: '#fff'}}
            rows={tapeFlow}
            columns={tradeColumns}
            throttleRowsMs={0}
            rowHeight={38}
            disableSelectionOnClick
             />
        </div>
      </div>
    </div>

  ), [tapeFlow, lotFilter]
  );
}
