import React, {useMemo} from 'react';
import classes from './SelectedTickerInfo.module.css';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';

export default function SelectedTickerInfo(props){

  return useMemo(
      () => (
    <div className={classes.SelectedTickerInfo}>
    <Card sx={{ width: '30%' }}>
      <CardActionArea>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {props.ticker !== ' ' ? props.ticker : 'All Tickers'}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
    <Card sx={{ width: '30%' }}>
      <CardActionArea>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            Mark: {'$' + ((props.tickerQuoteData[0] + props.tickerQuoteData[1]) /2).toFixed(2)}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
    <Card sx={{ width: '30%' }}>
      <CardActionArea>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {'Bid: ' + props.tickerQuoteData[1].toFixed(2) + ' - Ask: ' + props.tickerQuoteData[0].toFixed(2)}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
    </div>
  ), [props.ticker,props.tickerQuoteData]
  )
}
